# README #

This repository contains code for a very simple IGB App that adds a single menu 
item to the IGB-Classic Tools menu.

### How do I get set up? ###

* Clone the repository.
* Open the project simple-igb-app in NetBeans.
* Build the project.
* Start IGB (version 9 or higher, less than 10).
* Within IGB, use the App Manager (Tools menu) to add project target directory as a new App Repository.
* The Simple IGB App will appear in the App Manager. Install it.
* Go back to IGB. **Select Tools > A Very Simple App** to run the App.

### Questions or Comments? ###

Contact:

* Ann Loraine - aloraine@uncc.edu or aloraine@gmail.com
