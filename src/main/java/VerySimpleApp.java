import aQute.bnd.annotation.component.Component;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.swing.JOptionPane;
import org.lorainelab.igb.menu.api.MenuBarEntryProvider;
import org.lorainelab.igb.menu.api.model.MenuBarParentMenu;
import org.lorainelab.igb.menu.api.model.MenuItem;

@Component(immediate = true)
public class VerySimpleApp implements MenuBarEntryProvider {
    
    private final String myText = "Very Simple IGB App";
    
    @Override
    public Optional<List<MenuItem>> getMenuItems() {
        MenuItem menuItem = new MenuItem(myText, (Void t) -> {
            JOptionPane.showMessageDialog(null,myText);
            return t;
        });
        return Optional.ofNullable(Arrays.asList(menuItem));
    }
    
    @Override
    public MenuBarParentMenu getMenuExtensionParent() {
        return MenuBarParentMenu.TOOLS;
    }
}